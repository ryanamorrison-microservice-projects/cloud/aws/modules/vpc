<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.0.0, < 2.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.37.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_vpc.vpc_generic](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_vpc_dns_hostnames"></a> [vpc\_dns\_hostnames](#input\_vpc\_dns\_hostnames) | whether or not the VPC has DNS hostname support, default: false | `bool` | `false` | no |
| <a name="input_vpc_dns_support"></a> [vpc\_dns\_support](#input\_vpc\_dns\_support) | whether or not the VPC has DNS support, default: true | `bool` | `true` | no |
| <a name="input_vpc_instance_tenancy"></a> [vpc\_instance\_tenancy](#input\_vpc\_instance\_tenancy) | tenancy option for instances launched into the VPC, default: 'default' (like provider) | `string` | `"default"` | no |
| <a name="input_vpc_ipv4_cidr"></a> [vpc\_ipv4\_cidr](#input\_vpc\_ipv4\_cidr) | non-overlapping VPC CIDRs | `string` | n/a | yes |
| <a name="input_vpc_net_metrics"></a> [vpc\_net\_metrics](#input\_vpc\_net\_metrics) | indicates whether Network Address Usage metrics are enabled for VPC, default: false | `bool` | `false` | no |
| <a name="input_vpc_tags"></a> [vpc\_tags](#input\_vpc\_tags) | a map of key-value pairs consisting of additional tags to set on the vpc | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_vpc_arn"></a> [vpc\_arn](#output\_vpc\_arn) | Amazon resource name of VPC |
| <a name="output_vpc_default_nacl_id"></a> [vpc\_default\_nacl\_id](#output\_vpc\_default\_nacl\_id) | the ID of the network ACL created by default on VPC creation |
| <a name="output_vpc_default_route_table_id"></a> [vpc\_default\_route\_table\_id](#output\_vpc\_default\_route\_table\_id) | the ID of the route table created by default on VPC creation |
| <a name="output_vpc_default_secgroup_id"></a> [vpc\_default\_secgroup\_id](#output\_vpc\_default\_secgroup\_id) | the ID of the security group created by default on VPC creation |
| <a name="output_vpc_dns_hostname_support"></a> [vpc\_dns\_hostname\_support](#output\_vpc\_dns\_hostname\_support) | whether or not the VPC has DNS hostname support |
| <a name="output_vpc_dns_support"></a> [vpc\_dns\_support](#output\_vpc\_dns\_support) | whether or not the VPC has DNS support |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | ID of the VPC |
| <a name="output_vpc_instance_tenancy"></a> [vpc\_instance\_tenancy](#output\_vpc\_instance\_tenancy) | tenancy of instances spin up within VPC |
| <a name="output_vpc_ipv6_assoc_id"></a> [vpc\_ipv6\_assoc\_id](#output\_vpc\_ipv6\_assoc\_id) | the association ID for the IPv6 CIDR block |
| <a name="output_vpc_ipv6_block_net_border_group"></a> [vpc\_ipv6\_block\_net\_border\_group](#output\_vpc\_ipv6\_block\_net\_border\_group) | the Network Border Group Zone name |
| <a name="output_vpc_main_route_table_id"></a> [vpc\_main\_route\_table\_id](#output\_vpc\_main\_route\_table\_id) | the ID of the main route table associated with this VPC |
| <a name="output_vpc_net_metrics"></a> [vpc\_net\_metrics](#output\_vpc\_net\_metrics) | whether network address usage metrics are enabled for the VPC |
| <a name="output_vpc_owner_id"></a> [vpc\_owner\_id](#output\_vpc\_owner\_id) | the ID of the AWS account that owns the VPC |
| <a name="output_vpc_tags"></a> [vpc\_tags](#output\_vpc\_tags) | all tags for this VPC |
<!-- END_TF_DOCS -->