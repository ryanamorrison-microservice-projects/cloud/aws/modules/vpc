resource "aws_vpc" "vpc_generic" {
  cidr_block                           = var.vpc_ipv4_cidr
  enable_dns_support                   = var.vpc_dns_support
  enable_dns_hostnames                 = var.vpc_dns_hostnames
  enable_network_address_usage_metrics = var.vpc_net_metrics
  instance_tenancy                     = var.vpc_instance_tenancy
  tags                                 = var.vpc_tags
}
