output "vpc_arn" {
  value       = aws_vpc.vpc_generic.arn
  description = "Amazon resource name of VPC"
}
output "vpc_id" {
  value       = aws_vpc.vpc_generic.id
  description = "ID of the VPC"
}
output "vpc_dns_support" {
  value       = aws_vpc.vpc_generic.enable_dns_support
  description = "whether or not the VPC has DNS support"
}
output "vpc_net_metrics" {
  value       = aws_vpc.vpc_generic.enable_network_address_usage_metrics
  description = "whether network address usage metrics are enabled for the VPC"
}
output "vpc_dns_hostname_support" {
  value       = aws_vpc.vpc_generic.enable_dns_hostnames
  description = "whether or not the VPC has DNS hostname support"
}
output "vpc_main_route_table_id" {
  value       = aws_vpc.vpc_generic.main_route_table_id
  description = "the ID of the main route table associated with this VPC"
}
output "vpc_default_route_table_id" {
  value       = aws_vpc.vpc_generic.default_route_table_id
  description = "the ID of the route table created by default on VPC creation"
}
output "vpc_default_nacl_id" {
  value       = aws_vpc.vpc_generic.default_network_acl_id
  description = "the ID of the network ACL created by default on VPC creation"
}
output "vpc_default_secgroup_id" {
  value       = aws_vpc.vpc_generic.default_security_group_id
  description = "the ID of the security group created by default on VPC creation"
}
output "vpc_ipv6_assoc_id" {
  value       = aws_vpc.vpc_generic.ipv6_association_id
  description = "the association ID for the IPv6 CIDR block"
}
output "vpc_ipv6_block_net_border_group" {
  value       = aws_vpc.vpc_generic.ipv6_cidr_block_network_border_group
  description = "the Network Border Group Zone name"
}
output "vpc_owner_id" {
  value       = aws_vpc.vpc_generic.owner_id
  description = "the ID of the AWS account that owns the VPC"
}
output "vpc_tags" {
  value       = aws_vpc.vpc_generic.tags_all
  description = "all tags for this VPC"
}
output "vpc_instance_tenancy" {
  value       = aws_vpc.vpc_generic.instance_tenancy
  description = "tenancy of instances spin up within VPC"
}
