package test

import(
  "fmt"
  "github.com/gruntwork-io/terratest/modules/aws"
  "github.com/gruntwork-io/terratest/modules/terraform"
  "github.com/stretchr/testify/assert"
  "testing"
)

var awsRegion string = "us-west-2"
var vpcCIDR string = "10.255.0.0/16"
var wantVpcTags = map[string]string{ 
  "Name": "unit-test-vpc",
  "Environment": "test",
}

func TestVpc(t *testing.T) {
  t.Parallel()
  opts := &terraform.Options{
    TerraformDir: "../",
    Vars: map[string]interface{}{
      "vpc_ipv4_cidr": vpcCIDR,
      "vpc_tags": wantVpcTags,
    },
    EnvVars: map[string]string {
      "AWS_DEFAULT_REGION": awsRegion,
    },
  }
  defer terraform.Destroy(t, opts)
  terraform.InitAndApply(t, opts)

  vpcId := terraform.OutputRequired(t, opts, "vpc_id")
  gotVpcTags := aws.GetTagsForVpc(t, vpcId, awsRegion)
  //test debugging
  fmt.Println(vpcId)
  fmt.Println(gotVpcTags)
  assert.Equal(t, gotVpcTags["Environment"], wantVpcTags["Environment"])
}  
