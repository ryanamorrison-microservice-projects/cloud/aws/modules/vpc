#===========================
# REQUIRED
#===========================
variable "vpc_ipv4_cidr" {
  type        = string
  description = "non-overlapping VPC CIDRs"
}
#===========================
# OPTIONAL
#===========================
#defaults on next two vars follow:
#https://docs.aws.amazon.com/vpc/latest/userguide/vpc-dns.html#vpc-dns-support
variable "vpc_dns_support" {
  type        = bool
  description = "whether or not the VPC has DNS support, default: true"
  default     = true
}
variable "vpc_dns_hostnames" {
  type        = bool
  description = "whether or not the VPC has DNS hostname support, default: false"
  default     = false
}
variable "vpc_tags" {
  type        = map(string)
  description = "a map of key-value pairs consisting of additional tags to set on the vpc"
  default     = {}
}
variable "vpc_net_metrics" {
  type        = bool
  description = "indicates whether Network Address Usage metrics are enabled for VPC, default: false"
  default     = false
}
variable "vpc_instance_tenancy" {
  type        = string
  description = "tenancy option for instances launched into the VPC, default: 'default' (like provider)"
  default     = "default"
}
